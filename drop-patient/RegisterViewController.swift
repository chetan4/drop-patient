//
//  RegisterViewController.swift
//  drop-patient
//
//  Created by Idyllic on 7/5/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import MBProgressHUD


class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var profileBgView: UIImageView!
    @IBOutlet weak var cornerView: DataView!
    @IBOutlet weak var shadowView: DataView!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileBgView.layoutIfNeeded()
        let gradient = CAGradientLayer()
        
        
        gradient.frame = self.profileBgView.bounds
        gradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        gradient.startPoint = CGPoint.zero
        gradient.endPoint = CGPoint.init(x: 1, y: 1)
        
        self.profileBgView.layer.insertSublayer(gradient, at: 0)
        
        self.shadowView.layer.cornerRadius = 6.0
        self.shadowView.clipsToBounds =  true
        self.cornerView.backgroundColor = .clear
        
        self.registerButton.layoutIfNeeded()
        let buttonGradient = CAGradientLayer()
        buttonGradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        buttonGradient.startPoint = CGPoint.zero
        buttonGradient.endPoint = CGPoint.init(x: 1, y: 1)
        buttonGradient.frame = self.registerButton.bounds
        self.registerButton.layer.insertSublayer(buttonGradient, at: 0)
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       if textField == self.fullNameTF {
            self.emailTF.becomeFirstResponder()
        }
        else if textField == self.emailTF {
            self.passwordTF.becomeFirstResponder()
        }
        else if textField == self.passwordTF {
            self.mobileTF.becomeFirstResponder()
        }
        else if textField == self.mobileTF {
            self.registerAction(self.registerButton)
        }
        return true
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func takePhotoAction(_ sender: Any) {
        
    }
    
    @IBAction func signInAction(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVCId") as! LogInViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func registerAction(_ sender: Any) {
       if (self.fullNameTF.text?.characters.count)! < 1 {
            self.alertWithMessage(message: VALID_FULL_NAME_ERROR)
        }
        else
        if !self.isValidEmail(email: self.emailTF.text!) {
            self.alertWithMessage(message: VALID_EMAIL_ERROR)
        }
        else if (self.passwordTF.text?.characters.count)! < 1 {
            self.alertWithMessage(message: PASS_LENGTH_ERROR)
        }
        else if (self.passwordTF.text?.characters.count)! < 8 {
            self.alertWithMessage(message: PASS_VALIDATION_ERROR)
        }
        else if (self.mobileTF.text?.characters.count)! != 10 {
            self.alertWithMessage(message: VALID_MOBILE_NUMBER_ERROR)
        }
        else{
            let user = User()
            //separate the first name and last name from fullname textfield
            let nameArray = self.fullNameTF.text?.components(separatedBy: " ")
            if (nameArray?.count)! > 1 {
                user.firstName = (nameArray?[0])!
                user.lastName = (nameArray?[1])!
            }
            else if (nameArray?.count)! > 0{
                user.firstName = (nameArray?[0])!
                user.lastName = ""
            }
            else {
                user.firstName = self.fullNameTF.text!
                user.lastName = ""
            }
            user.mobileNumber = self.mobileTF.text!
            user.email = self.emailTF.text!
            user.password = self.passwordTF.text!
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            APIManager.sharedInstance.registerUser(user: user, completion: { (result:Dictionary<String, Bool>?, error:String?) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if result?["result"] == true{
                    print("Success")
                    
                    let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "dashboardVCId") as! DashboardViewController
                    self.navigationController?.pushViewController(dashboardVC, animated: true)

                    //                    self.switchToDashboard()
                }
                else{
                    self.alertWithMessage(message: error!)
                }
            })
        }
    }
}
