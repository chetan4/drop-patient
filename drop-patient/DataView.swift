//
//  DataView.swift
//  drop-patient
//
//  Created by Idyllic on 7/6/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class DataView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.dropShadow()
        self.addBorder()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.dropShadow()
        self.addBorder()
    }
    
    func addBorder() {
        self.layer.borderWidth = 0.3
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func dropShadow(scale: Bool = true) {
        self.backgroundColor = .white
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.layer.shadowRadius = 3
    }
}
