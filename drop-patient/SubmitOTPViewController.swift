//
//  SubmitOTPViewController.swift
//  drop-patient
//
//  Created by Idyllic on 7/12/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import MBProgressHUD
class SubmitOTPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var otpTF: UITextField!
    @IBOutlet weak var cornerView: DataView!
    @IBOutlet weak var shadowView: DataView!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var submitButton: UIButton!
    
    var phone_number: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let totalHeight = self.view.bounds.height
        self.logoTopConstraint.constant = 13.0 / 100 * totalHeight  // 13% of totalHeight
        self.cornerView.layer.cornerRadius = 6.0
        self.cornerView.clipsToBounds = true
        self.shadowView.backgroundColor = .clear
        
        self.submitButton.layoutIfNeeded()
        let buttonGradient = CAGradientLayer()
        buttonGradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        buttonGradient.startPoint = CGPoint.zero
        buttonGradient.endPoint = CGPoint.init(x: 1, y: 1)
        buttonGradient.frame = self.submitButton.bounds
        self.submitButton.layer.insertSublayer(buttonGradient, at: 0)

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.otpTF {
            self.submitOTPAction(self.submitButton)
            textField.resignFirstResponder()
        }
        return true
    }

    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func submitOTPAction(_ sender: Any) {
        if (self.otpTF.text?.characters.count)! != 6 {
            self.alertWithMessage(message: VALID_OTP_ERROR)
        }
        else{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            APIManager.sharedInstance.verifyOTP(otp:self.otpTF.text!, completion: { (result:Dictionary<String, Bool>?, error:String?) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if result?["result"] == true{
                    let resetpassVC = self.storyboard?.instantiateViewController(withIdentifier: "resetpassVCId") as! ResetPassViewController
                    resetpassVC.phone_number = self.phone_number
                    self.navigationController?.pushViewController(resetpassVC, animated: true)
                }
                else{
                    self.alertWithMessage(message: error!)
                }
            })
        }
    }
}
