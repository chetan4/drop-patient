//
//  APIManager.swift
//  drop-patient
//
//  Created by Idyllic on 7/9/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class APIManager: NSObject {
    static let sharedInstance = APIManager()
    
    func registerUser(user: User, completion: @escaping (_ result: Dictionary<String,Bool>?, _ error: String?) -> Void) {
        
        let hosturl = "\(hostName)\(registrationAPI)"
        let params = ["email":user.email, "password":user.password , "phone_number":user.mobileNumber ]
        
        Alamofire.request(hosturl, method: .post, parameters: ["patient":params], encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response:DataResponse<Any>) in
        
            switch(response.result) {
            case .success(_):
                if let value = response.result.value {
                    let json = JSON(value)
                    if json["success"] == true {
                        DispatchQueue.main.async {
                            completion(["result":true],nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(nil,DEFAULT_ERROR)
                        }
                    }
                }
                else{
                    completion(nil,(response.error?.localizedDescription)!)
                }
                break
            case .failure(_):
                completion(nil,(response.error?.localizedDescription)!)
                break
            }
        })
    }
    
    func loginUser(phone_number: String, password: String , completion: @escaping (_ result: Dictionary<String,Bool>?, _ error: String?) -> Void) {
        
        let hosturl = "\(hostName)\(loginAPI)"
        let params = ["phone_number":phone_number, "password": password]
        
        Alamofire.request(hosturl, method: .post, parameters: ["patient":params], encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let value = response.result.value {
                    let json = JSON(value)
                    if json["success"] == true {
                        DispatchQueue.main.async {
                            completion(["result":true],nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(nil,DEFAULT_ERROR)
                        }
                    }
                }
                else{
                    completion(nil,(response.error?.localizedDescription)!)
                }
                break
            case .failure(_):
                completion(nil,(response.error?.localizedDescription)!)
                break
            }
        })
    }

    func forgotPass(phone_number: String, completion: @escaping (_ result: Dictionary<String,Bool>?, _ error: String?) -> Void) {
        
        let hosturl = "\(hostName)\(forgotPassAPI)?phone_number=\(phone_number)"
        Alamofire.request(hosturl, method:.get, encoding: JSONEncoding.default)
            .responseJSON {
                (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if let value = response.result.value {
                        let json = JSON(value)
                        if json["success"] == true {
                            DispatchQueue.main.async {
                                completion(["result":true],nil)
                            }
                        }
                        else if let error = json["error"].string{
                            DispatchQueue.main.async {
                                completion(nil,error)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                completion(nil,DEFAULT_ERROR)
                            }
                        }
                    }
                    else{
                        completion(nil,(response.error?.localizedDescription)!)
                    }
                    break
                case .failure(_):
                    completion(nil,(response.error?.localizedDescription)!)
                    break
            }
        }
    }
    
    func verifyOTP(otp: String, completion: @escaping (_ result: Dictionary<String,Bool>?, _ error: String?) -> Void) {
        
        let hosturl = "\(hostName)\(verifyotpAPI)?otp=\(otp)"
        Alamofire.request(hosturl, method:.get, encoding: JSONEncoding.default)
            .responseJSON {
                (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if let value = response.result.value {
                        let json = JSON(value)
                        if json["success"] == true {
                            DispatchQueue.main.async {
                                completion(["result":true],nil)
                            }
                        }
                        else if let error = json["error"].string{
                            DispatchQueue.main.async {
                                completion(nil,error)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                completion(nil,DEFAULT_ERROR)
                            }
                        }
                    }
                    else{
                        completion(nil,(response.error?.localizedDescription)!)
                    }
                    break
                case .failure(_):
                    completion(nil,(response.error?.localizedDescription)!)
                    break
                }
        }
    }

    func resetPass(phone_number: String, password: String ,  completion: @escaping (_ result: Dictionary<String,Bool>?, _ error: String?) -> Void) {
        
        let hosturl = "\(hostName)\(resetPassAPI)"
        let params = ["phone_number":phone_number, "password": password]
       
        
        Alamofire.request(hosturl, method:.put, parameters: params, encoding: JSONEncoding.default)
            .responseJSON {
                (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if let value = response.result.value {
                        let json = JSON(value)
                        if json["success"] == true {
                            DispatchQueue.main.async {
                                completion(["result":true],nil)
                            }
                        }
                        else if let error = json["error"].string{
                            DispatchQueue.main.async {
                                completion(nil,error)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                completion(nil,DEFAULT_ERROR)
                            }
                        }
                    }
                    else{
                        completion(nil,(response.error?.localizedDescription)!)
                    }
                    break
                case .failure(_):
                    completion(nil,(response.error?.localizedDescription)!)
                    break
                }
        }
    }

}
