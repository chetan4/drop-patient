//
//  User.swift
//  drop-patient
//
//  Created by Idyllic on 7/9/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class User: NSObject {

    static var currentUser = User()
    
    var userID = Int()
    var email = String()
    var firstName = String()
    var lastName = String()
    var mobileNumber = String()
    var password = String()
}
