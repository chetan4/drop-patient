//
//  Constants.swift
//  drop-patient
//
//  Created by Idyllic on 7/9/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class Constants: NSObject {

}

//Alert Messages

let DEFAULT_ERROR: String = "Ooops! This is embarrasing. But something's not right. Can you come back after some time?"

let VALID_FULL_NAME_ERROR = "Please enter full name."
let VALID_MOBILE_NUMBER_ERROR = "That's not a valid mobile number."
let VALID_EMAIL_ERROR: String = "Email format doesn't seem to be correct."
let PASS_LENGTH_ERROR: String = "That's not a valid password."
let PASS_VALIDATION_ERROR: String = "Password must contain atleast six character."
let VALID_OLD_PASS_ERROR: String = "Your old password seems to be incorrect."
let VALID_NEW_PASS_ERROR: String = "That's not a valid new password."
let DONT_HAVE_ACC_STRING: String = "Don't have an account? "
let OTP_SENT = "OTP is sent on your registered mobile number!"
let VALID_OTP_ERROR = "This is not valid OTP."

let hostName = "http://54.83.143.201"
//End Points

let registrationAPI = "/register"
let loginAPI = "/login"
let forgotPassAPI = "/forgot_patients_password"
let verifyotpAPI = "/verify_patients_otp"
let resetPassAPI = "/reset_patient_password"

