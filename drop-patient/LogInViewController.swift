//
//  LoginViewController.swift
//  Docster
//
//  Created by Idyllic on 29/06/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import MBProgressHUD

class LogInViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shadowView: DataView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var cornerView: DataView!
    
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let totalHeight = self.view.bounds.height
        self.logoTopConstraint.constant = 13.0 / 100 * totalHeight  // 13% of totalHeight
        self.cornerView.layer.cornerRadius = 6.0
        self.cornerView.clipsToBounds = true
        self.shadowView.backgroundColor = .clear
        
        self.loginButton.layoutIfNeeded()
        let buttonGradient = CAGradientLayer()
        buttonGradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        buttonGradient.startPoint = CGPoint.zero
        buttonGradient.endPoint = CGPoint.init(x: 1, y: 1)
        buttonGradient.frame = self.loginButton.bounds
        self.loginButton.layer.insertSublayer(buttonGradient, at: 0)
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func forgotPassAction(_ sender: Any) {
        let forgotPassVC = self.storyboard?.instantiateViewController(withIdentifier: "forgotPassVCId") as! ForgotPassViewController
        self.navigationController?.pushViewController(forgotPassVC, animated: true)
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "registerVCId") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.mobileTF {
            self.passwordTF.becomeFirstResponder()
        }
        else if textField == self.passwordTF {
            textField.resignFirstResponder()
            self.loginAction(self.loginButton)
        }
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if (self.mobileTF.text?.characters.count)! != 10 {
            self.alertWithMessage(message: VALID_MOBILE_NUMBER_ERROR)
        }
        else if (self.passwordTF.text?.characters.count)! < 1 {
            self.alertWithMessage(message: PASS_LENGTH_ERROR)
        }
        else if (self.passwordTF.text?.characters.count)! < 6 {
            self.alertWithMessage(message: PASS_VALIDATION_ERROR)
        }
        else{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            APIManager.sharedInstance.loginUser(phone_number: mobileTF.text!, password: passwordTF.text!, completion: {
                (result:Dictionary<String, Bool>?, error:String?) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if result?["result"] == true{
                    let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "dashboardVCId") as! DashboardViewController
                    self.navigationController?.pushViewController(dashboardVC, animated: true)
//                    self.switchToDashboard()
                }
                else{
                    self.alertWithMessage(message: error!)
                }
            })
        }
    }
}
