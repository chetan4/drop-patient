//
//  Helper.swift
//  drop-patient
//
//  Created by Idyllic on 7/9/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class Helper: NSObject {

}

extension UIViewController {
    
    func alertWithMessage(message: String) {
        
        let alertController = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "Alright", style: .default, handler: {(alert: UIAlertAction!) in
        })
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func isValidEmail(email:String) -> Bool {
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", regEx)
        
        return predicate.evaluate(with: email)
        
    }
}
