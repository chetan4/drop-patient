//
//  PJNavigationController.swift
//  drop-patient
//
//  Created by Idyllic on 7/6/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class PJNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationBar.isHidden = true
    }

}
