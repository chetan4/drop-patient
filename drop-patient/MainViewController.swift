//
//  ViewController.swift
//  drop-patient
//
//  Created by Idyllic on 6/29/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signInButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var getStartedButtonTopConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let totalHeight = self.view.bounds.height
        self.logoTopConstraint.constant = 12.9 / 100 * totalHeight
        self.signInButtonBottomConstraint.constant = 1.8 / 100 * totalHeight
        self.getStartedButtonTopConstraint.constant = 80.4 / 100 * totalHeight
    }
    @IBAction func gettingStartedAction(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "registerVCId") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func signInAction(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVCId") as! LogInViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
}

