//
//  ResetPassViewController.swift
//  drop-patient
//
//  Created by Idyllic on 7/12/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import MBProgressHUD

class ResetPassViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var cornerView: DataView!
    @IBOutlet weak var shadowView: DataView!
    @IBOutlet weak var resetPassButton: UIButton!
    
    var phone_number: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let totalHeight = self.view.bounds.height
        self.logoTopConstraint.constant = 13.0 / 100 * totalHeight  // 13% of totalHeight
        self.cornerView.layer.cornerRadius = 6.0
        self.cornerView.clipsToBounds = true
        self.shadowView.backgroundColor = .clear
        
        self.resetPassButton.layoutIfNeeded()
        let buttonGradient = CAGradientLayer()
        buttonGradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        buttonGradient.startPoint = CGPoint.zero
        buttonGradient.endPoint = CGPoint.init(x: 1, y: 1)
        buttonGradient.frame = self.resetPassButton.bounds
        self.resetPassButton.layer.insertSublayer(buttonGradient, at: 0)

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.passwordTF {
            self.resetPassAction(self.resetPassButton)
            textField.resignFirstResponder()
        }
        return true
    }

    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func resetPassAction(_ sender: Any) {
        if (self.passwordTF.text?.characters.count)! < 1 {
            self.alertWithMessage(message: PASS_LENGTH_ERROR)
        }
        else if (self.passwordTF.text?.characters.count)! < 6 {
            self.alertWithMessage(message: PASS_VALIDATION_ERROR)
        }
        else{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            APIManager.sharedInstance.resetPass(phone_number: self.phone_number, password: passwordTF.text!, completion: {
                (result:Dictionary<String, Bool>?, error:String?) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if result?["result"] == true{
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginVCId") as! LogInViewController
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                else{
                    self.alertWithMessage(message: error!)
                }
            })
        }
    }

}
