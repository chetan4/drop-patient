//
//  ForgotPassViewController.swift
//  drop-patient
//
//  Created by Idyllic on 7/7/17.
//  Copyright © 2017 Idyllic. All rights reserved.
//

import UIKit
import MBProgressHUD

class ForgotPassViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var cornerView: DataView!
    @IBOutlet weak var shadowView: DataView!
    @IBOutlet weak var forgotPassButton: UIButton!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mobileTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let totalHeight = self.view.bounds.height
        self.logoTopConstraint.constant = 13.0 / 100 * totalHeight  // 13% of totalHeight
        self.cornerView.layer.cornerRadius = 6.0
        self.cornerView.clipsToBounds = true
        self.shadowView.backgroundColor = .clear
        
        self.forgotPassButton.layoutIfNeeded()
        let buttonGradient = CAGradientLayer()
        buttonGradient.colors = [UIColor.drpFadedOrange.cgColor , UIColor.drpLipstick.cgColor]
        buttonGradient.startPoint = CGPoint.zero
        buttonGradient.endPoint = CGPoint.init(x: 1, y: 1)
        buttonGradient.frame = self.forgotPassButton.bounds
        self.forgotPassButton.layer.insertSublayer(buttonGradient, at: 0)
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.mobileTF {
            self.forgotPassAction(self.forgotPassButton)
            textField.resignFirstResponder()
        }
        return true
    }

    @IBAction func forgotPassAction(_ sender: Any) {
        if (self.mobileTF.text?.characters.count)! != 10 {
            self.alertWithMessage(message: VALID_MOBILE_NUMBER_ERROR)
        }
        else{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            APIManager.sharedInstance.forgotPass(phone_number:self.mobileTF.text!, completion: { (result:Dictionary<String, Bool>?, error:String?) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if result?["result"] == true{
                    let submitotpVC = self.storyboard?.instantiateViewController(withIdentifier: "submitotpVCId") as! SubmitOTPViewController
                    submitotpVC.phone_number = self.mobileTF.text!
                    self.navigationController?.pushViewController(submitotpVC, animated: true)
                }
                else{
                    self.alertWithMessage(message: error!)
                }
            })
        }
    }
}
